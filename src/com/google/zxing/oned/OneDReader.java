/*
 * Copyright 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.oned;

import android.util.Log;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Encapsulates functionality and implementation that is common to all families
 * of one-dimensional barcodes.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Sean Owen
 */
public abstract class OneDReader implements Reader {

	public static boolean testrow = false;

	@Override
	public List<Result> decode(BinaryBitmap image, List<Result> resultList) throws NotFoundException,
			ChecksumException, FormatException {
		// TODO Auto-generated method stub
		return decode(image, null, resultList);
	}

	@Override
	public void reset() {
		// do nothing
	}

	@Override
	public List<Result> decode(BinaryBitmap image, Map<DecodeHintType, ?> hints, List<Result> resultList)
			throws NotFoundException, ChecksumException, FormatException {
		// TODO Auto-generated method stub
		return doDecode(image, hints, resultList);
	}

	/**
	 * We're going to examine rows from the middle outward, searching
	 * alternately above and below the middle, and farther out each time.
	 * rowStep is the number of rows between each successive attempt above and
	 * below the middle. So we'd scan row middle, then middle - rowStep, then
	 * middle + rowStep, then middle - (2 * rowStep), etc. rowStep is bigger as
	 * the image is taller, but is always at least 1. We've somewhat arbitrarily
	 * decided that moving up and down by about 1/16 of the image is pretty
	 * good; we try more of the image if "trying harder".
	 *
	 * @param image
	 *            The image to decode
	 * @param hints
	 *            Any hints that were requested
	 * @return The contents of the decoded barcode
	 * @throws FormatException
	 * @throws ChecksumException
	 * @throws NotFoundException
	 *             Any spontaneous errors which occur
	 */
	private List<Result> doDecode(BinaryBitmap image,
			Map<DecodeHintType, ?> hints, List<Result> resultList) throws NotFoundException,
			ChecksumException, FormatException {
		int width = image.getWidth();
		int height = image.getHeight();
		int leftRow = width * 2 / 3;
		BitArray row = new BitArray(width);

		// 设置扫描起始位置为 图片开头处 去除开头部分
		int top = height >> 5;
		int rowStep = Math.max(1, height >> 5);
		int maxLines = height;
		// 判断列表中是否存在 该result
		boolean myResultIsExist = false;
		// 自上而下的扫描一次
		int rowNumber = top + rowStep;
		// 扫描左侧 的三条
		for (int x = 0; x < maxLines; x++) {

			if (rowNumber >= height) {
				// if we run off bottom, stop
				break;
			}

			// Estimate black point for this row and load it:
			try {
				// 找到一行中的黑色点
				row = image.getBlackRow(rowNumber, row);
				row = new BitArray(row.getBitArray(), leftRow);
			} catch (NotFoundException ignored) {
				continue;
			}
			rowNumber += rowStep;

			// While we have the image data in a BitArray, it's fairly cheap to
			// reverse it in place to
			// handle decoding upside down barcodes.
			for (int attempt = 0; attempt < 2; attempt++) {
				try {
					// Look for a barcode
					Result result = decodeRow(rowNumber, row, hints);
					ResultPoint[] points = result.getResultPoints();
					if (points != null) {
						Log.d("decoderowpoints",
								String.valueOf(points[1].getX()));
					}
					Log.d("decoderowresults", result.getText());
					Log.d("decoderowsize", String.valueOf(row.getSize()));
					// 添加到 列表里
					for (Result myResult : resultList) {
						// 查看 扫描到的文本是否存在 存在 则不添加 否则添加
						if ((myResult.getText().trim()).equals(result.getText().trim())) {
							myResultIsExist = true;
							break;
						} else {
							myResultIsExist = false;
						}
					}
					// 如果不存在 则 加入列表
					if (!myResultIsExist) {
						resultList.add(result);
					}
				} catch (ReaderException re) {
					// continue -- just couldn't decode this row
				}

			}
		}
		// 扫描右侧 的两条
		// 初始化 起始点
		rowNumber = top + rowStep;
		for (int x = 0; x < maxLines; x++) {

			if (rowNumber >= height) {
				// if we run off bottom, stop
				break;
			}

			// Estimate black point for this row and load it:
			try {
				// 找到一行中的黑色点
				row = image.getBlackRow(rowNumber, row);
				// 反转一次将 后面数据放到最前面
				row.reverse();
				// 将新的数据生成新的数组
				row = new BitArray(row.getBitArray(), leftRow);
				// 再次反转将数据 按正确顺序排列
				row.reverse();
			} catch (NotFoundException ignored) {
				continue;
			}
			rowNumber += rowStep;

			// While we have the image data in a BitArray, it's fairly cheap to
			// reverse it in place to
			// handle decoding upside down barcodes.
			for (int attempt = 0; attempt < 2; attempt++) {
				try {
					// Look for a barcode
					Result result = decodeRow(rowNumber, row, hints);
					ResultPoint[] points = result.getResultPoints();
					if (points != null) {
						Log.d("decoderowpoints",
								String.valueOf(points[1].getX()));
					}
					Log.d("decoderowresults", result.getText());
					Log.d("decoderowsize", String.valueOf(row.getSize()));
					// 添加到 列表里
					for (Result myResult : resultList) {
						// 查看 扫描到的文本是否存在 存在 则不添加 否则添加
						if (myResult.getText().equals(result.getText())) {
							myResultIsExist = true;
							break;
						} else {
							myResultIsExist = false;
						}
					}
					// 如果不存在 则 加入列表
					if (!myResultIsExist) {
						resultList.add(result);
					}
				} catch (ReaderException re) {
					// continue -- just couldn't decode this row
				}

			}
		}

		return resultList;

	}

	/**
	 * Records the size of successive runs of white and black pixels in a row,
	 * starting at a given point. The values are recorded in the given array,
	 * and the number of runs recorded is equal to the size of the array. If the
	 * row starts on a white pixel at the given start point, then the first
	 * count recorded is the run of white pixels starting from that point;
	 * likewise it is the count of a run of black pixels if the row begin on a
	 * black pixels at that point.
	 *
	 * @param row
	 *            row to count from
	 * @param start
	 *            offset into row to start at
	 * @param counters
	 *            array into which to record counts
	 * @throws NotFoundException
	 *             if counters cannot be filled entirely from row before running
	 *             out of pixels
	 */
	protected static void recordPattern(BitArray row, int start, int[] counters)
			throws NotFoundException {
		int numCounters = counters.length;
		Arrays.fill(counters, 0, numCounters, 0);
		int end = row.getSize();
		if (start >= end) {
			throw NotFoundException.getNotFoundInstance();
		}
		boolean isWhite = !row.get(start);
		int counterPosition = 0;
		int i = start;
		while (i < end) {
			if (row.get(i) ^ isWhite) { // that is, exactly one is true
				counters[counterPosition]++;
			} else {
				counterPosition++;
				if (counterPosition == numCounters) {
					break;
				} else {
					counters[counterPosition] = 1;
					isWhite = !isWhite;
				}
			}
			i++;
		}
		// If we read fully the last section of pixels and filled up our
		// counters -- or filled
		// the last counter but ran off the side of the image, OK. Otherwise, a
		// problem.
		if (!(counterPosition == numCounters || (counterPosition == numCounters - 1 && i == end))) {
			throw NotFoundException.getNotFoundInstance();
		}
	}

	protected static void recordPatternInReverse(BitArray row, int start,
			int[] counters) throws NotFoundException {
		// This could be more efficient I guess
		int numTransitionsLeft = counters.length;
		boolean last = row.get(start);
		while (start > 0 && numTransitionsLeft >= 0) {
			if (row.get(--start) != last) {
				numTransitionsLeft--;
				last = !last;
			}
		}
		if (numTransitionsLeft >= 0) {
			throw NotFoundException.getNotFoundInstance();
		}
		recordPattern(row, start + 1, counters);
	}

	/**
	 * Determines how closely a set of observed counts of runs of black/white
	 * values matches a given target pattern. This is reported as the ratio of
	 * the total variance from the expected pattern proportions across all
	 * pattern elements, to the length of the pattern.
	 *
	 * @param counters
	 *            observed counters
	 * @param pattern
	 *            expected pattern
	 * @param maxIndividualVariance
	 *            The most any counter can differ before we give up
	 * @return ratio of total variance between counters and pattern compared to
	 *         total pattern size
	 */
	protected static float patternMatchVariance(int[] counters, int[] pattern,
			float maxIndividualVariance) {
		int numCounters = counters.length;
		int total = 0;
		int patternLength = 0;
		for (int i = 0; i < numCounters; i++) {
			total += counters[i];
			patternLength += pattern[i];
		}
		if (total < patternLength) {
			// If we don't even have one pixel per unit of bar width, assume
			// this is too small
			// to reliably match, so fail:
			return Float.POSITIVE_INFINITY;
		}

		float unitBarWidth = (float) total / patternLength;
		maxIndividualVariance *= unitBarWidth;

		float totalVariance = 0.0f;
		for (int x = 0; x < numCounters; x++) {
			int counter = counters[x];
			float scaledPattern = pattern[x] * unitBarWidth;
			float variance = counter > scaledPattern ? counter - scaledPattern
					: scaledPattern - counter;
			if (variance > maxIndividualVariance) {
				return Float.POSITIVE_INFINITY;
			}
			totalVariance += variance;
		}
		return totalVariance / total;
	}

	/**
	 * <p>
	 * Attempts to decode a one-dimensional barcode format given a single row of
	 * an image.
	 * </p>
	 *
	 * @param rowNumber
	 *            row number from top of the row
	 * @param row
	 *            the black/white pixel data of the row
	 * @param hints
	 *            decode hints
	 * @return {@link Result} containing encoded string and start/end of barcode
	 * @throws NotFoundException
	 *             if no potential barcode is found
	 * @throws ChecksumException
	 *             if a potential barcode is found but does not pass its
	 *             checksum
	 * @throws FormatException
	 *             if a potential barcode is found but format is invalid
	 */
	public abstract Result decodeRow(int rowNumber, BitArray row,
			Map<DecodeHintType, ?> hints) throws NotFoundException,
			ChecksumException, FormatException;

}
