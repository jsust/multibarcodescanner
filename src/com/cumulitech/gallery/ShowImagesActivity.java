package com.cumulitech.gallery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.cumulitech.iphonesellertool.IPhoneMapping;
import com.cumulitech.iphonesellertool.MultibarcodeScanActivity;
import com.cumulitech.iphonesellertool.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;
import android.widget.GridView;

public class ShowImagesActivity extends Activity {
	private HashMap<String, List<ImagesBean>> mGruopMap = new HashMap<String, List<ImagesBean>>();
	private final static int SCAN_OK = 1;
	private final static int DELETE_OK = 2;
	private ProgressDialog mProgressDialog;
	private ChildAdapter adapter;
	private GridView mGroupGridView;
	private List<ImagesBean> childList;
	private String EMAIL_SUBJECT = "Multiple ScanBar";
	/**
	 * iphone 机型 映像
	 */
	private IPhoneMapping iphoneMapping;

	private String _txtPath;
	/**
	 * 最大 可以发送的 图片数量
	 */
	protected final int MAX_SEND_IMG = 20;
	/**
	 * 返回 选择的图片 指定的位置
	 */
	private List<Integer> positionList;
	/**
	 * 用于全选 取消全选 flag
	 */
	public static boolean isChecked = false;
	/**
	 * 全选按钮对象
	 */
	public static Button selectAll;
	/**
	 * 包 资源对象
	 */
	public static Resources myPackageRes;
	// 可能存在内存泄漏 问题带解决
	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			switch (msg.what) {
			case SCAN_OK:
				// 关闭进度条
				mProgressDialog.dismiss();
				if (childList != null) {
					adapter = new ChildAdapter(ShowImagesActivity.this,
							childList, mGroupGridView);
					mGroupGridView.setAdapter(adapter);
				} else {
					setToast(ShowImagesActivity.this, "did not find pictures!",
							Toast.LENGTH_SHORT);

				}
				// 绑定 全选 事件
				((Button) findViewById(R.id.select_all))
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								// 全选 或取消选择
								selectOrCancel();
							}
						});
				break;
			case DELETE_OK:
				// 关闭进度条
				mProgressDialog.dismiss();
				// 重新加载 适配器 刷新界面
				Log.d("positionListsize", String.valueOf(childList.size()));
				adapter = new ChildAdapter(ShowImagesActivity.this, childList,
						mGroupGridView);
				mGroupGridView.setAdapter(adapter);
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 初始化本地语言
		Configuration config = getResources().getConfiguration();
		SharedPreferences sp = getSharedPreferences("default",
				MODE_PRIVATE);
		Locale last = new Locale(sp.getString("language",
				config.locale.getLanguage()), sp.getString("country",
				config.locale.getCountry()));
		config.locale = last;
		DisplayMetrics dm = getResources().getDisplayMetrics();
		getResources().updateConfiguration(config, dm);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.show_image_activity);
		_txtPath = Environment.getExternalStorageDirectory().getPath()
				+ "/MultiBarScanner/index.txt";
		// 配置标题栏的布局
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.title_bar);
		// 初始化用于显示的gridView
		mGroupGridView = (GridView) findViewById(R.id.show_image_grid);
		// 获取本包内的资源对象
		myPackageRes = getResources();
		// 全选 按钮
		selectAll = (Button) findViewById(R.id.select_all);
		// 创建 iphone 机型编码映像
		iphoneMapping = new IPhoneMapping();
		// 绑定back 按钮的返回事件
		((Button) findViewById(R.id.title_bar_back))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// 结束本Activity 返回上一界面	
						Intent intent = new Intent(ShowImagesActivity.this, MultibarcodeScanActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						ShowImagesActivity.this.finish();
					}
				});

		// 绑定发送 按钮的事件
		((Button) findViewById(R.id.send_email))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// 获取 adapter中选择的项 的列表
						if (childList != null) {
							positionList = adapter.getSelectItems();
							if (positionList.size() <= MAX_SEND_IMG) {
								Log.d("getSelectItemspositionList",
										String.valueOf(positionList.size()));
								sendEmail();
							} else {
								setToast(ShowImagesActivity.this,
										"Selected images must less than "
												+ MAX_SEND_IMG,
										Toast.LENGTH_LONG);
							}
						} else {
							setToast(ShowImagesActivity.this,
									"you did not select any picture!",
									Toast.LENGTH_SHORT);
						}
					}
				});
		// 绑定删除文件事件
		((Button) findViewById(R.id.delete_button))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (childList != null) {
							// 获取 adapter中选择的项 的列表
							positionList = adapter.getSelectItems();
							// 删除选中的文件
							deleteFiles();
						} else {
							setToast(ShowImagesActivity.this,
									"you did not select any picture!",
									Toast.LENGTH_SHORT);
						}
					}
				});
		// 搜索图片
		getImages();
	}

	/**
	 * 发送邮件
	 */
	private void sendEmail() {
		if (childList != null) {
			if ((positionList.size()) != 0) {
				Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
				// String[] tos = { "way.ping.li@gmail.com" };
				// String[] ccs = { "way.ping.li@gmail.com" };
				// intent.putExtra(Intent.EXTRA_EMAIL, tos);
				// intent.putExtra(Intent.EXTRA_CC, ccs);
				List<String> contensList = getEmailbody();
				Log.d("getSelectItemscontensList",
						String.valueOf(contensList.size()));
				String content = "Part No.      \tUPC               \tSerial No.          \tIMEI                       \tUnit Info\n";
				for (String item : contensList) {
					if (item != null) {
						// 读取 数据映像表 获取 机型数据
						String UNIT_INFO = iphoneMapping.getUnitInfo((item
								.split("\t"))[0]);
						Log.d("nullitem", item);
						content += (item + "\t" + UNIT_INFO + "\n");
					}
				}
				// 邮件的 正文
				intent.putExtra(Intent.EXTRA_TEXT, content);
				// 邮件的 主题
				intent.putExtra(Intent.EXTRA_SUBJECT, EMAIL_SUBJECT);

				ArrayList<Uri> imageUris = new ArrayList<Uri>();
				// 获取adapter中的 选中项位置 作为附件发送
				for (int i = 0; i < positionList.size(); i++) {
					imageUris
							.add(Uri.parse("file://"
									+ childList.get(positionList.get(i))
											.getImagePath()));
				}
				// imageUris.add(Uri.parse("file:///storage/sdcard0/MultiBarScanner/null.jpg"));
				// imageUris.add(Uri.parse("file:///storage/sdcard0/MultiBarScanner/null.jpg"));
				intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,
						imageUris);
				intent.setType("image/*");
				intent.setType("message/rfc882");
				// 弹出选择 哪种发送邮件 的客户端
				// Intent.createChooser(intent, "Choose Email Client");
				// 打开 gmail
				startActivity(intent);
			} else {
				setToast(ShowImagesActivity.this,
						"you did not select any picture!", Toast.LENGTH_SHORT);
			}
		} else {
			setToast(ShowImagesActivity.this, "Please select a picture!",
					Toast.LENGTH_LONG);
		}
	}

	/**
	 * 对所有 图片 全选 或 取消 选择
	 */
	@SuppressLint("UseSparseArrays")
	private void selectOrCancel() {
		if (childList != null) {
			// 创建一个 hashMap 存储选择项
			HashMap<Integer, Boolean> mSelectMap = new HashMap<Integer, Boolean>();
			Log.d("childList.size", String.valueOf(childList.size()));
			if (!isChecked) {
				// 全选将所有项 设为 选中
				for (int i = 0; i < childList.size(); i++) {
					mSelectMap.put(i, true);
				}
				isChecked = true;
				selectAll.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.button_select_selector));
			} else {
				// 取消所有选择
				for (int i = 0; i < childList.size(); i++) {
					mSelectMap.put(i, false);
				}
				isChecked = false;
				selectAll.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.button_selected_selector));
			}
			// 重新加载 适配器 刷新界面
			adapter = new ChildAdapter(ShowImagesActivity.this, childList,
					mGroupGridView, mSelectMap);
			mGroupGridView.setAdapter(adapter);
		} else {
			setToast(ShowImagesActivity.this, "There is no picture!",
					Toast.LENGTH_SHORT);
		}
	}

	/**
	 * 写 email正文
	 */
	private List<String> getEmailbody() {
		Log.d("getSelectItemspositionListsize",
				String.valueOf(positionList.size()));
		Log.d("getSelectchildList", String.valueOf(childList.size()));
		List<String> list = new ArrayList<String>();
		for (int item : positionList) {
			Log.d("clipContentsList",
					String.valueOf(item) + "||"
							+ (childList.get(item)).getFileName());
			// 获取一行对应的值 没有检索到结果时 返回空
			String itemcontent = getImageContent(_txtPath,
					(childList.get(item)).getFileName());
			// 值获取有结果的值
			if (itemcontent != "") {
				// bug solve：item is a item of the list positionList
				list.add(itemcontent);
			}
		}
		return list;

	}

	/**
	 * 获取txt文本中的 对应图片 指定内容
	 */
	private String getImageContent(String txtPath, String imageName) {
		String contents = readFileSdcard(txtPath, imageName);
		Log.d("contents", contents);
		return contents;

	}

	/**
	 * 删除选择文件
	 * 
	 * @param file
	 */
	public void deleteFiles() {

		if (childList != null) {
			if (positionList.size() != 0) {
				// 显示进度条
				mProgressDialog = ProgressDialog
						.show(this, null, "Deleting...");
				new Thread(new Runnable() {
					// 将全局数据 positionList取回本地 防止对全局的 数据改动
					List<Integer> localPositionList = positionList;

					@Override
					public void run() {
						for (int i = 0; i < localPositionList.size(); i++) {
							int item = localPositionList.get(i);
							Log.d("localPositionListItems",
									String.valueOf(item));
							File file = new File(childList.get(item)
									.getImagePath());
							// 判断文件是否存在
							if (file.exists()) {
								// 删除 本地的文件
								file.delete();
								if (!file.exists()) {
									// 删除媒体库中的对应数据
									ContentResolver mContentResolver = ShowImagesActivity.this
											.getContentResolver();
									mContentResolver
											.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
													MediaStore.Images.Media.DATA
															+ "=?",
													new String[] { childList
															.get(item)
															.getImagePath() });
									// 删除 index.txt 文件记录
									deleteRecord(_txtPath, childList.get(item)
											.getFileName());
									// 删除list列表数据
									// 这样做的话 这里出现了一个问题 remove 是删除了 对应项的key和value
									// 也就意味着 所有 后面的数都往前移动一位，list 的size 减少了1 会报
									// 数组越界的异常
									// 但是 我们又不能保留这个空Key位 后面要重新加载 这个Childlist 空位
									// 会出现占位的图片
									// 将其设定为一个标志位null
									childList.set(item, null);
									Log.d("positionListsize",
											String.valueOf(localPositionList
													.size())
													+ "||"
													+ String.valueOf(childList
															.size()));
								} else {
									setToast(ShowImagesActivity.this, childList
											.get(item).getFileName()
											+ " delete failed!",
											Toast.LENGTH_SHORT);
								}
							} else {
								setToast(ShowImagesActivity.this, childList
										.get(item).getFileName()
										+ " does not exist!",
										Toast.LENGTH_SHORT);
							}
						}
						// 新建一个 存放ImagesBean 容器
						List<ImagesBean> newChildList = new ArrayList<ImagesBean>();
						// 去除标志位 null项
						for (ImagesBean _item : childList) {
							if (_item != null) {
								newChildList.add(_item);
							}
						}
						// 赋值给 childList
						childList = newChildList;

						// 通知Handler扫描图片完成
						mHandler.sendEmptyMessage(DELETE_OK);
					}
				}).start();
			} else {
				setToast(ShowImagesActivity.this, "Please select a picture!",
						Toast.LENGTH_SHORT);
			}
		} else {
			setToast(ShowImagesActivity.this, "Please select a picture!",
					Toast.LENGTH_SHORT);
		}
	}

	/**
	 * 删除 index.txt 记录
	 */
	private void deleteRecord(String txtPath, String fileName) {
		Log.d("clipContentsList", "ok");
		// 读取文件index.txt 的内容 去除重复的 IMEI 并存入一个list
		List<String> contentsList = readToList(txtPath, fileName);
		String newContents = "";
		File file = new File(txtPath);
		// 删除当前存在的文件 之后在创建
		if (file.exists()) {
			file.delete();
		}
		for (String contents : contentsList) {
			newContents = newContents + contents + "\n";
		}
		// 写入 对应路径的内存卡中
		MultibarcodeScanActivity.writeFileSdcard(txtPath, newContents);
	}

	/**
	 * 读取 index.txt 内容 到list
	 * 
	 * @param filePath
	 * @param imageName
	 * @return
	 */
	public List<String> readToList(String filePath, String imageName) {
		String encoding = "GBK";
		int i = 0;
		List<Integer> indexList = new ArrayList<Integer>();
		List<String> indexContentsList = new ArrayList<String>();
		Log.d("clipContentsList", "ok");
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					// 将读取的一行数据存储到列表中
					indexContentsList.add(lineTxt);
					Log.d("clipContentsList", lineTxt);
					List<String> list = clipContents(lineTxt);
					// 判断所读的 一行中 是否有对应的 图片名存在 如果存在则取出这一行
					for (String item : list) {
						Log.d("clipContentsList", item + "||" + imageName);
						if (item.trim().equals(imageName.trim())) {
							// 将重复的一条记录 存储在列表中
							indexList.add(i);

						}
					}
					i++;
				}
				i = 0;
				// 关闭读取
				read.close();
				Log.d("indexListSize", String.valueOf(indexContentsList.size())
						+ "||" + String.valueOf(indexList.size()));

				if (indexList.size() != 0) {
					// 去除列表中重复的记录
					for (int index : indexList) {
						indexContentsList.set(index, null);
					}
					List<String> newList = new ArrayList<String>();
					for (String item : indexContentsList) {
						if (item != null) {
							newList.add(item);
						}
					}

					indexContentsList = newList;
					Log.d("indexListSize",
							String.valueOf(indexContentsList.size()));
				}
				indexList.clear();
			} else {

				// ShowImagesActivity.setToast(CaptureActivity.this,
				// "cannot find the file!", Toast.LENGTH_LONG);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexContentsList;
	}

	/**
	 * 读在/mnt/sdcard/目录下面的文件
	 * 
	 * @param fileName
	 * @return
	 */

	public String readFileSdcard(String fileName, String imageName) {
		String encoding = "GBK";
		String res = "";
		try {
			File file = new File(fileName);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					Log.d("clipContentsList", lineTxt);
					List<String> list = clipContents(lineTxt);

					// 判断所读的 一行中 是否有对应的 图片名存在 如果存在则取出这一行
					for (String item : list) {
						Log.d("clipContentsList", item + "||" + imageName);
						if (item.trim().equals(imageName.trim())) {
							res = lineTxt;
						}
					}
				}
				read.close();
			} else {

				setToast(ShowImagesActivity.this, "cannot find the file!",
						Toast.LENGTH_LONG);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;

	}

	/**
	 * 按照存储到 index.txt文档中的格式 分割内容
	 */
	public static List<String> clipContents(String contents) {

		String[] arr = (contents.split("\t"));
		List<String> list = new ArrayList<String>();
		if (arr.length != 0) {
			list = Arrays.asList(arr);
		}
		return list;
	}

	/**
	 * 弹出消息的
	 * 
	 * @param message
	 * @param time
	 */
	public static void setToast(Context context, String message, int time) {
		Toast.makeText(context, message, time).show();
	}

	/**
	 * 利用ContentProvider扫描手机中的图片，此方法在运行在子线程中
	 */
	private void getImages() {
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			setToast(this, "暂无外部存储", Toast.LENGTH_SHORT);
			return;
		}

		// 显示进度条
		mProgressDialog = ProgressDialog.show(this, null, "Loading...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				// String myGalleryUri =
				// "content://media/external/images/media/";
				// Uri mImageUri = Uri.parse(myGalleryUri);
				Uri mImageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				ContentResolver mContentResolver = ShowImagesActivity.this
						.getContentResolver();

				// 只查询jpeg的图片
				Cursor mCursor = mContentResolver.query(mImageUri, null,
						MediaStore.Images.Media.MIME_TYPE + "=?",
						new String[] { "image/jpeg" },
						MediaStore.Images.Media.DATE_MODIFIED);

				while (mCursor.moveToNext()) {
					// 获取图片的路径
					String path = mCursor.getString(mCursor
							.getColumnIndex(MediaStore.Images.Media.DATA));
					Log.d("path", path);

					// 获取该图片的父路径名
					String parentName = new File(path).getParentFile()
							.getName();
					Log.d("parentName", parentName);
					// 指定文件夹为MultiBarScanner中
					if (parentName.equals("MultiBarScanner")) {

						// 获取文件名字
						String fileName = new File(path).getName();
						// “.”和“|”都是转义字符，必须得加"\\";
						fileName = (fileName.split("\\."))[0];

						ImagesBean image = new ImagesBean();
						image.setImagePath(path);
						image.setFileName(fileName);
						Log.d("setFileName", path + "||" + fileName);
						// 根据父路径名将图片放入到mGruopMap中
						if (!mGruopMap.containsKey(parentName)) {
							List<ImagesBean> chileList = new ArrayList<ImagesBean>();
							chileList.add(image);
							mGruopMap.put(parentName, chileList);
						} else {
							mGruopMap.get(parentName).add(image);
						}
					}
				}

				mCursor.close();
				// 将文件夹MultiBarScanner里的结果取出来
				// 用于向适配器里添加
				childList = mGruopMap.get("MultiBarScanner");
				// 通知Handler扫描图片完成
				mHandler.sendEmptyMessage(SCAN_OK);
			}
		}).start();
	}

}
