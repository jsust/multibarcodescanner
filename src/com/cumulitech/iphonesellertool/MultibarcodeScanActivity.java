package com.cumulitech.iphonesellertool;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulitech.camera.CameraManager;
import com.cumulitech.gallery.ShowImagesActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

/**
 * This activity opens the camera and does the actual scanning on a background
 * thread. It draws a viewfinder to help the user place the barcode correctly,
 * shows feedback as the image processing is happening, and then overlays the
 * results when a scan is successful.
 * 
 * 此Activity所做的事： 1. 开启camera，在后台独立线程中完成扫描任务； 2.
 * 绘制了一个扫描区（viewfinder）来帮助用户将条码置于其中以准确扫描； 3. 扫描成功后会将扫描结果展示在界面上。
 * 
 * @author Sean Owen
 */
@SuppressLint("WorldWriteableFiles") public final class MultibarcodeScanActivity extends Activity implements
		Camera.AutoFocusCallback, SurfaceHolder.Callback {

	private static final String TAG = MultibarcodeScanActivity.class
			.getSimpleName();

	public static final int HISTORY_REQUEST_CODE = 0x0000bacc;

	private CameraManager cameraManager;

	private MultibarcodeScanActivityHandler handler;

	private Result savedResultToShow;

	private ViewfinderView viewfinderView;
	/**
	 * 扫描提示，例如"请将条码置于取景框内扫描"之类的提示
	 */
	private TextView statusView;

	/**
	 * 扫描结果展示窗口
	 */
	private View resultView;

	private boolean hasSurface;

	/**
	 * 【辅助解码的参数(用作MultiFormatReader的参数)】 编码类型，该参数告诉扫描器采用何种编码方式解码，即EAN-13，QR
	 * Code等等 对应于DecodeHintType.POSSIBLE_FORMATS类型
	 * 参考DecodeThread构造函数中如下代码：hints.put(DecodeHintType.POSSIBLE_FORMATS,
	 * decodeFormats);
	 */
	private Collection<BarcodeFormat> decodeFormats;

	/**
	 * 【辅助解码的参数(用作MultiFormatReader的参数)】 字符集，告诉扫描器该以何种字符集进行解码
	 * 对应于DecodeHintType.CHARACTER_SET类型
	 * 参考DecodeThread构造器如下代码：hints.put(DecodeHintType.CHARACTER_SET,
	 * characterSet);
	 */
	private String characterSet;

	/**
	 * 【辅助解码的参数(用作MultiFormatReader的参数)】 该参数最终会传入MultiFormatReader，
	 * 上面的decodeFormats和characterSet最终会先加入到decodeHints中 最终被设置到MultiFormatReader中
	 * 参考DecodeHandler构造器中如下代码：multiFormatReader.setHints(hints);
	 */
	private Map<DecodeHintType, ?> decodeHints;

	/**
	 * 活动监控器。如果手机没有连接电源线，那么当相机开启后如果一直处于不被使用状态则该服务会将当前activity关闭。
	 * 活动监控器全程监控扫描活跃状态，与CaptureActivity生命周期相同。每一次扫描过后都会重置该监控，即重新倒计时。
	 */

	//
	// /**
	// * 声音震动管理器。 扫描成功后可以播放提示音并震动，这两种功能都是用户自定义的。 在Barcode
	// * Scanner中点击菜单键，点设置即可看到这两项的设置
	// */
	// private BeepManager beepManager;

	/**
	 * 获取条形码显示结果内容布局，用于添加 和 清空
	 */
	private LinearLayout content_layout;
	/**
	 * 定时关闭结果处理机制
	 */
	private Handler result_handler;

	protected boolean isFirst;
	/**
	 * sd卡根目录变量
	 */
	private String SDcardPath;
	/**
	 * 存储rawresult
	 * 
	 */
	private List<RawResult> resultsList;

	/**
	 * 打开图库
	 * 
	 * @return
	 */
	private Button galleryBunton;
	/**
	 * 退出
	 * 
	 * @return
	 */
	private Button exitBunton;
	/**
	 * 语言切换
	 * 
	 * @return
	 */
	private Button languageBunton;
	/**
	 * 确认对话框
	 */
	protected String dialog_confirm;

	/**
	 * preferences 中存放的 格式 项
	 */
	protected final String FILE_FORMAT = "file_format";
	/**
	 * 读取历史记录的最大条数
	 */
	private final int MAX_RECORD_NUM = 12;
	/**
	 * MultiBarScanner 文件路径
	 */
	public static String _filePath;
	/**
	 * index.txt路径
	 */
	public String txtPath;
	/**
	 * 进入 结果显示页面后 重新扫描的时间 单位为毫秒
	 */
	private final long RESCAN_TIME = 1500;
	/**
	 * 第一次刚登入程序时防止动画加载
	 */
	private boolean NOT_FIRST_SHOW_RECENT;
	/**
	 * 最近记录linear布局 变量
	 */
	private LinearLayout recentHistory;
	private List<String> contentsList = new ArrayList<String>();

	/************ variable and function boundary ************/

	ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	CameraManager getCameraManager() {
		return cameraManager;
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		// 初始化本地语言
		Configuration config = getResources().getConfiguration();
		SharedPreferences sp = getSharedPreferences("default",
				MODE_PRIVATE);		
		Locale last = new Locale(sp.getString("language",
				config.locale.getLanguage()), sp.getString("country",
				config.locale.getCountry()));
		// 设置本地默认语言 为之后的弹出对话框上的选项初始化
		Locale.setDefault(last);
		Log.d("defaultLanguage",Locale.getDefault().toString());
		config.locale = last; 
		DisplayMetrics dm = getResources().getDisplayMetrics();
		getResources().updateConfiguration(config, dm);
		
		// 在扫描功能开启后，保持屏幕处于点亮状态
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.multibarcode);

		// 这里仅仅是对各个组件进行简单的创建动作，真正的初始化动作放在onResume中
		hasSurface = false;
		// Sd卡 根目录
		SDcardPath = Environment.getExternalStorageDirectory().getPath();
		// 拼接路径
		_filePath = SDcardPath + "/MultiBarScanner/";
		txtPath = _filePath + "index.txt";
		cameraManager = new CameraManager(getApplication());
		resultsList = new ArrayList<RawResult>();
		// beepManager = new BeepManager(this);
	}

	@SuppressLint("HandlerLeak")
	@Override
	protected void onResume() {
		super.onResume();
		// variable must be initialized here, not in onCreate(). This is
		// necessary because we don't
		// want to open the camera driver and measure the screen size if we're
		// going to show the help on
		// first launch. That led to bugs where the scanning rectangle was the
		// wrong size and partially
		// off screen.
		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		viewfinderView.setCameraManager(cameraManager);
		resultView = findViewById(R.id.result_view);
		statusView = (TextView) findViewById(R.id.status_view);
		galleryBunton = (Button) findViewById(R.id.gallery_button);
		exitBunton = (Button) findViewById(R.id.exit_button);
		languageBunton = (Button) findViewById(R.id.language_button);
		recentHistory = (LinearLayout) findViewById(R.id.recent_history);
		handler = null;

		// 进入图库
		loadGalleryButtonListener();
		// 退出
		loadExitButtonListener();
		result_handler = new Handler() {
			@Override
			public void handleMessage(Message msg) { // handle message
				switch (msg.what) {
				case 1:
					restartPreviewAfterDelay(1500);
					// 显示一条新的历史记录
					showHistory();
				}

				super.handleMessage(msg);
			}
		};
		// 触碰 聚焦
		initViewEvent();
		// 摄像头预览功能必须借助SurfaceView，因此也需要在一开始对其进行初始化
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			// The activity was paused but not stopped, so the surface still
			// exists. Therefore
			// surfaceCreated() won't be called, so init the camera here.
			initCamera(surfaceHolder);
		} else {
			// Install the callback and wait for surfaceCreated() to init the
			// camera.
			// 如果SurfaceView已经渲染完毕，会回调surfaceCreated，在surfaceCreated中调用initCamera()
			surfaceHolder.addCallback(this);
		}
		// 加载声音配置，其实在BeemManager的构造器中也会调用该方法，即在onCreate的时候会调用一次
		// beepManager.updatePrefs();
		// 在主界面显示 历史记录
		showHistory();
		// 设置语言切换事件
		languageBunton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showLanguageSelector();

			}
		});

	}

	// 语言选择的操作代码
	public void changeLanguage(Locale loc) {
		Resources resources = MultibarcodeScanActivity.this.getResources();// 获得res资源对象
		Configuration config = resources.getConfiguration();// 获得设置对象
		DisplayMetrics dm = resources.getDisplayMetrics();// 获得屏幕参数：主要是分辨率，像素等。
		config.locale = loc; // 配置本地语言
		resources.updateConfiguration(config, dm);
		// statusView.setText(R.string.msg_default_status);
		getSharedPreferences("default", MODE_WORLD_WRITEABLE).edit()
				.putString("language", config.locale.getLanguage())
				.putString("country", config.locale.getCountry()).commit();
		Intent intent = getIntent();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	// 弹出单选对话框 选择 菜单
	public void showLanguageSelector() {
		int i = 0;
		switch(Locale.getDefault().toString().trim()){
		case "en": i = 0;
		break;
		case "zh_CN": i = 1;
		break;
		case "zh_TW": i = 2;
		break;
		}
		new AlertDialog.Builder(this)
				.setTitle(
						getResources()
								.getString(R.string.Language_dialog_titel))
				.setSingleChoiceItems(
						new String[] {
								getResources().getString(
										R.string.Language_dialog_en),
								getResources().getString(
										R.string.Language_dialog_zh_CN),
								getResources().getString(
										R.string.Language_dialog_zh_TW) }, i,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								switch (which) {
								case 0:changeLanguage(Locale.ENGLISH);
								break;
								case 1:changeLanguage(Locale.SIMPLIFIED_CHINESE);
								break;
								case 2:changeLanguage(Locale.TRADITIONAL_CHINESE);
								break;
								}
							}
						})
				.setNegativeButton(
						getResources().getString(
								R.string.exit_dialog_notify_cancel), null)
				.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}

		// 关闭摄像头
		cameraManager.closeDriver();
		if (!hasSurface) {
			SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			surfaceHolder.removeCallback(this);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_FOCUS:
		case KeyEvent.KEYCODE_CAMERA:
			// Handle these events so they don't launch the Camera app
			return true;
			// Use volume up/down to turn on light
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			cameraManager.setTorch(false);
			return true;
		case KeyEvent.KEYCODE_VOLUME_UP:
			cameraManager.setTorch(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.close();

		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

	}

	/**
	 * 向CaptureActivityHandler中发送消息，并展示扫描到的图像
	 * 
	 * @param bitmap
	 * @param result
	 */
	private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
		// Bitmap isn't used yet -- will be used soon
		if (handler == null) {
			savedResultToShow = result;
		} else {
			if (result != null) {
				savedResultToShow = result;
			}
			if (savedResultToShow != null) {
				Message message = Message.obtain(handler,
						R.id.decode_succeeded, savedResultToShow);
				handler.sendMessage(message);
			}
			savedResultToShow = null;
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (holder == null) {
			Log.e(TAG,
					"*** WARNING *** surfaceCreated() gave us a null surface!");
		}
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	/**
	 * A valid barcode has been found, so give an indication of success and show
	 * the results.
	 * 
	 * @param scaleFactor
	 *            amount by which thumbnail was scaled
	 * @param barcode
	 *            A greyscale bitmap of the camera data which was decoded.
	 */
	public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
		boolean fromLiveScan = barcode != null;
		if (fromLiveScan) {
			// Then not from history, so beep/vibrate and we have an image to
			// draw on
			// beepManager.playBeepSoundAndVibrate(); // 扫描成功鸣叫
			// drawResultPoints(barcode, scaleFactor, rawResult);
		}

		handleDecodeInternally(rawResult, barcode);

	}

	// Put up our own UI for how to handle the decoded contents.
	/**
	 * 该方法会将最终处理的结果展示到result_view上。
	 * 
	 * 
	 * @param rawResult
	 * @param resultHandler
	 * @param barcode
	 */
	private void handleDecodeInternally(Result rawResult, Bitmap barcode) {
		Map<String, String> codeMap = null;
		// 初始化各个码
		String PN_CODE = null;
		String SN_CODE = null;
		String IMEI_CODE = null;
		String UPC_CODE = null;
		String format = rawResult.getBarcodeFormat().toString();
		String content = rawResult.getText();

		Log.d("_results", format + ":" + content);

		contentsList.add(content);
		// 将每个扫码和条码结果 存储到一个数组中 之后 取出使用
		resultsList.add(new RawResult(format, content));
		// 限制扫描的结果数量
		if (resultsList.size() >= DecodeHandler.BARCODE_NUM) {
			// 结果显示界面的 结果
			content_layout = (LinearLayout) findViewById(R.id.contents_Layout);
			// 清楚resultview 里的textview
			content_layout.removeAllViews();

			codeMap = judgeCode(contentsList);
			PN_CODE = codeMap.get("PN_CODE");
			SN_CODE = codeMap.get("SN_CODE");
			IMEI_CODE = codeMap.get("IMEI_CODE");
			UPC_CODE = codeMap.get("UPC_CODE");

			if (IMEI_CODE != null) {
				// 隐藏 几个 显示控件
				statusView.setVisibility(View.GONE);
				viewfinderView.setVisibility(View.GONE);
				recentHistory.setVisibility(View.GONE);
				// 显示结果布局
				resultView.setVisibility(View.VISIBLE);
				ImageView barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);
				if (barcode == null) {
					barcodeImageView.setImageBitmap(BitmapFactory
							.decodeResource(getResources(),
									R.drawable.ic_launcher));
				} else {
					// 向 imageView里放扫描的图片
					barcodeImageView.setImageBitmap(barcode);
					String savedFilePath = _filePath + IMEI_CODE + ".jpg";
					try {
						saveBitmapToFile(barcode, savedFilePath);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String newContents = PN_CODE + "\t" + UPC_CODE + "\t"
							+ SN_CODE + "\t" + IMEI_CODE + "\n";
					// 读取文件index.txt 的内容 去除重复的 IMEI 并存入一个list
					List<String> contentsList = readToList(txtPath, IMEI_CODE);
					File file = new File(txtPath);
					// 删除当前存在的文件 之后在创建
					if (file.exists()) {
						file.delete();
					}
					for (String contents : contentsList) {
						if (contents != null) {
							newContents = newContents + contents + "\n";
						}
					}
					// 写入 对应路径的内存卡中
					writeFileSdcard(txtPath, newContents);
					// 发送 扫描更新gallery
					requestScanFile(MultibarcodeScanActivity.this, new File(
							savedFilePath));
				}

				DateFormat formatter = DateFormat.getDateTimeInstance(
						DateFormat.SHORT, DateFormat.SHORT);
				TextView timeTextView = (TextView) findViewById(R.id.time_text_view);
				timeTextView.setText(formatter.format(new Date(rawResult
						.getTimestamp())));
				for (RawResult _result : resultsList) {
					TextView contentsTextView = new TextView(
							content_layout.getContext());
					contentsTextView.setTextSize(20L);
					contentsTextView.setText(_result.getContent());
					content_layout.addView(contentsTextView);
				}
				resultsList.clear();
				// 计时1.5秒钟重新扫描
				Message message = result_handler.obtainMessage(1);
				// Message
				result_handler.sendMessageDelayed(message, RESCAN_TIME);
				Log.d("CpatureActivity", "send Message success!");
			}

		}

	}

	/**
	 * 判断 条码类型并返回 map
	 */
	public static Map<String, String> judgeCode(List<String> resultList) {
		Map<String, String> codeMap = new HashMap<String, String>();
		// 初始化为null
		codeMap.put("IMEI_CODE", null);
		codeMap.put("ICCID_CODE", null);
		codeMap.put("UPC_CODE", null);
		codeMap.put("PN_CODE", null);
		codeMap.put("SN_CODE", null);
		String OTHER_CODE = null;
		for (String barCode : resultList) {
			if (barCode != null) {
				switch ((barCode.trim()).length()) {
				case 15:
					codeMap.put("IMEI_CODE", barCode);
					break;
				case 20:
					codeMap.put("ICCID_CODE", barCode);
					break;
				case 12:
					codeMap.put("UPC_CODE", barCode.substring(0, 11));
					break;
				default:
					OTHER_CODE = barCode;
				}
				if (OTHER_CODE != null) {
					if (OTHER_CODE.substring(OTHER_CODE.length() - 2).equals(
							"/A")) {
						codeMap.put("PN_CODE", OTHER_CODE.substring(2));
					} else {
						codeMap.put("SN_CODE", OTHER_CODE.substring(1));
					}
					OTHER_CODE = null;
				}
			}
		}
		return codeMap;

	}

	/**
	 * Read each line to the list and remove the same item
	 */
	public List<String> readToList(String filePath, String imageName) {
		String encoding = "GBK";
		int i = 0;
		List<Integer> indexList = new ArrayList<Integer>();
		List<String> indexContentsList = new ArrayList<String>();
		Log.d("clipContentsList", "ok");
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					// 将读取的一行数据存储到列表中
					indexContentsList.add(lineTxt);
					Log.d("clipContentsList", lineTxt);
					List<String> list = ShowImagesActivity
							.clipContents(lineTxt);
					// 判断所读的 一行中 是否有对应的 图片名存在 如果存在则取出这一行
					for (String item : list) {
						Log.d("clipContentsList", item + "||" + imageName);
						if (item.trim().equals(imageName.trim())) {
							// 将重复的一条记录 存储在列表中
							indexList.add(i);
						}
					}
					i++;
				}
				i = 0;
				// 关闭读取
				read.close();
				Log.d("indexListSize", String.valueOf(indexContentsList.size())
						+ "||" + String.valueOf(indexList.size()));

				if (indexList.size() != 0) {
					// 去除列表中重复的记录
					for (int index : indexList) {
						indexContentsList.set(index, null);
					}
					List<String> newList = new ArrayList<String>();
					for (String item : indexContentsList) {
						if (item != null) {
							Log.d("indexListSizeitem", item);
							newList.add(item);
						}
					}

					indexContentsList = newList;
					Log.d("indexListSize",
							String.valueOf(indexContentsList.size()));
				}
				indexList.clear();
			} else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexContentsList;
	}

	/**
	 * 发送扫描广播 更新gallery
	 * 
	 * @param context
	 * @param file
	 */
	public static void requestScanFile(Context context, File file) {
		Intent i = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		i.setData(Uri.fromFile(file));
		context.sendBroadcast(i);
	}

	@Override
	public void closeContextMenu() {
		// TODO Auto-generated method stub

		super.closeContextMenu();
	}

	@Override
	public void onContextMenuClosed(Menu menu) {
		// TODO Auto-generated method stub

		super.onContextMenuClosed(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		if (surfaceHolder == null) {
			throw new IllegalStateException("No SurfaceHolder provided");
		}
		if (cameraManager.isOpen()) {
			Log.w(TAG,
					"initCamera() while already open -- late SurfaceView callback?");
			return;
		}
		try {
			cameraManager.openDriver(surfaceHolder);
			// Creating the handler starts the preview, which can also throw a
			// RuntimeException.
			if (handler == null) {
				handler = new MultibarcodeScanActivityHandler(this,
						decodeFormats, decodeHints, characterSet, cameraManager);
			}
			decodeOrStoreSavedBitmap(null, null);
		} catch (IOException ioe) {
			Log.w(TAG, ioe);
			displayFrameworkBugMessageAndExit();
		} catch (RuntimeException e) {
			// Barcode Scanner has seen crashes in the wild of this variety:
			// java.?lang.?RuntimeException: Fail to connect to camera service
			Log.w(TAG, "Unexpected error initializing camera", e);
			displayFrameworkBugMessageAndExit();
		}
	}

	/**
	 * 删除resultview中的 textView
	 */
	public void removeResultsTextView() {
		content_layout = (LinearLayout) findViewById(R.id.contents_Layout);
		// content_layout.removeAllViews();
		int count = content_layout.getChildCount();
		Log.d("chidren", String.valueOf(count));
		if (count != 0) {
			content_layout.removeViews(0, count);
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		NOT_FIRST_SHOW_RECENT = false;
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		NOT_FIRST_SHOW_RECENT = false;
	}

	private void displayFrameworkBugMessageAndExit() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.app_name));
		builder.setMessage(getString(R.string.msg_camera_framework_bug));
		builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
		builder.setOnCancelListener(new FinishListener(this));
		builder.show();
	}

	/**
	 * 在经过一段延迟后重置相机以进行下一次扫描。 成功扫描过后可调用此方法立刻准备进行下次扫描
	 * 
	 * @param delayMS
	 */
	public void restartPreviewAfterDelay(long delayMS) {
		if (handler != null) {
			handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}
		resetStatusView();
	}

	/**
	 * 展示状态视图和扫描窗口，隐藏结果视图
	 */
	private void resetStatusView() {
		// scanbackButton.setVisibility(View.VISIBLE);
		resultView.setVisibility(View.GONE);
		statusView.setText(R.string.msg_default_status);
		statusView.setVisibility(View.VISIBLE);
		viewfinderView.setVisibility(View.VISIBLE);
		// 显示最近记录组建
		recentHistory.setVisibility(View.VISIBLE);

	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}

	/**
	 * 触碰 聚焦
	 */
	public void initViewEvent() {
		// torch事件会带有手势的动作执行 按下 起来 会产生两次事件 又是会出现异常bug
		// viewfinderView.setOnTouchListener(new OnTouchListener() {
		//
		// @Override
		// public boolean onTouch(View arg0, MotionEvent arg1) {
		// // TODO Auto-generated method stub
		// cameraManager.getCamera().autoFocus(
		// MultibarcodeScanActivity.this);
		// return true;
		// }
		// });
		viewfinderView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cameraManager.getCamera().autoFocus(
						MultibarcodeScanActivity.this);
			}
		});
	}

	@Override
	public void onAutoFocus(boolean arg0, Camera arg1) {
		// TODO Auto-generated method stub
		if (arg0) {
			// 聚焦成功时调用onPreviewFrame预览
			cameraManager.getCamera().setOneShotPreviewCallback(
					cameraManager.getPreviewCallback());
			cameraManager.getCamera().cancelAutoFocus();
		} else {
			// 如果聚焦失败就再次聚焦
			cameraManager.getCamera().autoFocus(MultibarcodeScanActivity.this);
		}
	}

	/**
	 * 向指定的index.txt文件中写入一条数据
	 * 
	 * @param fileName
	 * @param message
	 */
	// 写入/mnt/sdcard/index.txt
	public static void writeFileSdcard(String filePath, String message) {
		try {
			// FileOutputStream(String name, boolean append)
			FileOutputStream fout = new FileOutputStream(filePath, true);
			byte[] bytes = message.getBytes();
			fout.write(bytes);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save Bitmap to a file
	 * 
	 * @param bitmap
	 * @param file
	 * @return error message if the saving is failed. null if the saving is
	 *         successful.
	 * @throws IOException
	 */
	public static void saveBitmapToFile(Bitmap bitmap, String _file)
			throws IOException {
		BufferedOutputStream os = null;
		try {
			File file = new File(_file);
			// String _filePath_file.replace(File.separatorChar +
			// file.getName(), "");
			int end = _file.lastIndexOf(File.separator);
			String _filePath = _file.substring(0, end);
			File filePath = new File(_filePath);
			if (!filePath.exists()) {
				filePath.mkdirs();
			}
			file.createNewFile();
			os = new BufferedOutputStream(new FileOutputStream(file));
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					Log.e("save images", e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * 加载 退出事件
	 */
	private void loadExitButtonListener() {
		exitBunton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// 退出
				exitApp();
			}
		});
	}

	/**
	 * 在界面 显示最近记录
	 */
	@SuppressLint("NewApi")
	private void showHistory() {
		// 获取 IMEI后四位 数据
		List<String> list = readRecentlyHistory(txtPath);
		// 首先清除 原有里面的记录
		recentHistory.removeAllViews();
		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				String item = list.get(i);
				TextView contentsTextView = new TextView(
						recentHistory.getContext());
				contentsTextView.setTextColor(Color.WHITE);
				contentsTextView.setGravity(Gravity.CENTER);
				// contentsTextView.setBackgroundColor(getResources().getColor(R.color.textviewbg_color));
				contentsTextView.setBackgroundDrawable(getResources()
						.getDrawable(R.drawable.textview_bg));
				// 首次登入时不做动画响应
				if (NOT_FIRST_SHOW_RECENT) {
					if (i == 0) {
						Animation myAnimation_Scale = AnimationUtils
								.loadAnimation(this, R.anim.my_scale_action);
						contentsTextView.startAnimation(myAnimation_Scale);
					}
				} else {
					NOT_FIRST_SHOW_RECENT = true;
				}
				contentsTextView.setTextSize(20L);
				contentsTextView.setText(item);
				recentHistory.addView(contentsTextView);
			}
		} else {
			NOT_FIRST_SHOW_RECENT = true;
		}
	}

	/**
	 * 读取最近的 几条记录
	 */
	private List<String> readRecentlyHistory(String filepath) {
		int i = 0;
		List<String> records = new ArrayList<String>();
		String encoding = "GBK";
		try {
			File file = new File(filepath);
			if (file.exists()) {
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String IMEICode = (ShowImagesActivity.clipContents(lineTxt))
							.get(3);
					if (IMEICode.length() == 15) {
						IMEICode = IMEICode.substring(11);
					}
					Log.d("readRecentlyHistory", lineTxt);
					records.add(IMEICode);
					i++;
					if (i >= MAX_RECORD_NUM) {
						break;
					}
				}
				i = 0;
				read.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			ShowImagesActivity.setToast(MultibarcodeScanActivity.this,
					"Get recent record failed!", Toast.LENGTH_SHORT);
			e.printStackTrace();
		}
		return records;
	}

	/**
	 * 进入 gallery
	 */
	private void loadGalleryButtonListener() {
		galleryBunton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// start gallery
				Intent intent = new Intent(MultibarcodeScanActivity.this,
						ShowImagesActivity.class);
				startActivity(intent);
			}
		});
	}

	/**
	 * 退出应用程序
	 */
	private void exitApp() {

		AlertDialog alertDialog = new AlertDialog.Builder(this)
				.setTitle(
						getResources().getString(
								R.string.exit_dialog_notify_caution))
				.setMessage(
						getResources().getString(
								R.string.exit_dialog_notify_notify))
				.setPositiveButton(
						getResources()
								.getString(R.string.exit_dialog_notify_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.addCategory(Intent.CATEGORY_HOME);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								android.os.Process
										.killProcess(android.os.Process.myPid());
							}

						})
				.setNegativeButton(
						getResources().getString(
								R.string.exit_dialog_notify_cancel),

						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								return;
							}
						}).create();
		alertDialog.show();
	}
}
