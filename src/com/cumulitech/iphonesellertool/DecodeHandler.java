package com.cumulitech.iphonesellertool;

import android.graphics.Bitmap;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class DecodeHandler extends Handler {

	private static final String TAG = DecodeHandler.class.getSimpleName();

	private final MultibarcodeScanActivity activity;

	private final MultiFormatReader multiFormatReader;

	private boolean running = true;

	private Map<DecodeHintType, Object> hints;
	/**
	 * 扫描的条形码条数
	 */
	public static final int BARCODE_NUM = 5;
	
	public List<Result> resultsList;

	DecodeHandler(MultibarcodeScanActivity activity,
			Map<DecodeHintType, Object> hints) {
		multiFormatReader = new MultiFormatReader();
		multiFormatReader.setHints(hints);
		this.activity = activity;
		this.hints = hints;
		resultsList = new ArrayList<Result>();

	}

	@Override
	public void handleMessage(Message message) {
		if (!running) {
			return;
		}
		switch (message.what) {
		case R.id.decode:
			decode((byte[]) message.obj, message.arg1, message.arg2);
			break;
		case R.id.quit:
			running = false;
			Looper.myLooper().quit();
			break;
		}
	}

	/**
	 * Decode the data within the viewfinder rectangle, and time how long it
	 * took. For efficiency, reuse the same reader objects from one decode to
	 * the next.
	 * 
	 * @param data
	 *            The YUV preview frame.
	 * @param width
	 *            The width of the preview frame.
	 * @param height
	 *            The height of the preview frame.
	 */
	private void decode(byte[] data, int width, int height) {
		long start = System.currentTimeMillis();
		Handler handler = activity.getHandler();
		resultsList = yuvModeDecode(data, width, height,resultsList);
			if (resultsList.size() >= BARCODE_NUM) {
				PlanarYUVLuminanceSource source = activity.getCameraManager()
						.buildLuminanceSource(data, width, height);
				// Don't log the barcode contents for security.
				long end = System.currentTimeMillis();
				Log.d(TAG, "Found barcode in " + (end - start) + " ms");
				Message message = null;
				// 循环将Result数组发送消息
				if (handler != null) {
					for (Result getResult : resultsList) {
						Log.d("resultscontent", getResult.getText());
						message = Message.obtain(handler, R.id.decode_wait,
								getResult);
						message.sendToTarget();
					}
					Log.d("text", String.valueOf(resultsList.size()));
					message = Message.obtain(handler, R.id.decode_succeeded);
					Bundle bundle = new Bundle();
					bundleThumbnail(source, bundle);
					message.setData(bundle);
					message.sendToTarget();
					// 发送成功后 清楚列表数据，为第二次发送准备
					resultsList.clear();
				}
			} else {
				if (handler != null) {
					Message message = Message.obtain(handler,
							R.id.decode_failed);
					message.sendToTarget();
				}
			}
	}

	public List<Result> yuvModeDecode(byte[] data, int width, int height, List<Result> resultsList) {
		PlanarYUVLuminanceSource source = activity.getCameraManager()
				.buildLuminanceSource(data, width, height);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		try {
			resultsList = multiFormatReader.decodeWithState(bitmap, resultsList);
			for(Result myResult : resultsList){
				Log.d("resulttext", myResult.getText());
			}
			
			
		} catch (ReaderException re) {
			// continue
		} finally {
			multiFormatReader.reset();
		}

		return resultsList;
	}

	private static void bundleThumbnail(PlanarYUVLuminanceSource source,
			Bundle bundle) {
		int[] pixels = source.renderThumbnail();
		int width = source.getThumbnailWidth();
		int height = source.getThumbnailHeight();
		Bitmap bitmap = Bitmap.createBitmap(pixels, 0, width, width, height,
				Bitmap.Config.ARGB_8888);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
		bundle.putByteArray(DecodeThread.BARCODE_BITMAP, out.toByteArray());
		bundle.putFloat(DecodeThread.BARCODE_SCALED_FACTOR, (float) width
				/ source.getWidth());
	}

}
