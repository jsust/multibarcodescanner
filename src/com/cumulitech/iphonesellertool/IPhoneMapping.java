package com.cumulitech.iphonesellertool;

import java.util.HashMap;
import java.util.Map;

public class IPhoneMapping {
	Map<String, String> unitInfoDB ;
	
	public IPhoneMapping() {
		unitInfoDB = new HashMap<String,String>();
		unitInfoDB.put("MG4P2LL/A", "iPhone 6,A1549 GSM,ATT,silver,16 GB");
		unitInfoDB.put("MG4X2LL/A", "iPhone 6,A1549 GSM,ATT,silver,64 GB");
		unitInfoDB.put("MG4U2LL/A", "iPhone 6,A1549 GSM,ATT,silver,128 GB");
		unitInfoDB.put("MG4Q2LL/A", "iPhone 6,A1549 GSM,ATT,gold,16 GB");
		unitInfoDB.put("MG502LL/A", "iPhone 6,A1549 GSM,ATT,gold,64 GB");
		unitInfoDB.put("MG4V2LL/A", "iPhone 6,A1549 GSM,ATT,gold,128 GB");
		unitInfoDB.put("MG4N2LL/A", "iPhone 6,A1549 GSM,ATT,space gray,16 GB");
		unitInfoDB.put("MG4W2LL/A", "iPhone 6,A1549 GSM,ATT,space gray,64 GB");
		unitInfoDB.put("MG4R2LL/A", "iPhone 6,A1549 GSM,ATT,space gray,128 GB");
		unitInfoDB.put("MG552LL/A", "iPhone 6,A1549 GSM,T-Mobile,silver,16 GB");
		unitInfoDB.put("MG5C2LL/A", "iPhone 6,A1549 GSM,T-Mobile,silver,64 GB");
		unitInfoDB.put("MG582LL/A", "iPhone 6,A1549 GSM,T-Mobile,silver,128 GB");
		unitInfoDB.put("MG562LL/A", "iPhone 6,A1549 GSM,T-Mobile,gold,16 GB");
		unitInfoDB.put("MG5D2LL/A", "iPhone 6,A1549 GSM,T-Mobile,gold,64 GB");
		unitInfoDB.put("MG592LL/A", "iPhone 6,A1549 GSM,T-Mobile,gold,128 GB");
		unitInfoDB.put("MG542LL/A", "iPhone 6,A1549 GSM,T-Mobile,space gray,16 GB");
		unitInfoDB.put("MG5A2LL/A", "iPhone 6,A1549 GSM,T-Mobile,space gray,64 GB");
		unitInfoDB.put("MG572LL/A", "iPhone 6,A1549 GSM,T-Mobile,space gray,128 GB");
		unitInfoDB.put("MG5X2LL/A", "iPhone 6,A1549 CDMA,Verizon,silver,16 GB");
		unitInfoDB.put("MG642LL/A", "iPhone 6,A1549 CDMA,Verizon,silver,64 GB");
		unitInfoDB.put("MG612LL/A", "iPhone 6,A1549 CDMA,Verizon,silver,128 GB");
		unitInfoDB.put("MG5Y2LL/A", "iPhone 6,A1549 CDMA,Verizon,gold,16 GB");
		unitInfoDB.put("MG652LL/A", "iPhone 6,A1549 CDMA,Verizon,gold,64 GB");
		unitInfoDB.put("MG622LL/A", "iPhone 6,A1549 CDMA,Verizon,gold,128 GB");
		unitInfoDB.put("MG5W2LL/A", "iPhone 6,A1549 CDMA,Verizon,space gray,16 GB");
		unitInfoDB.put("MG632LL/A", "iPhone 6,A1549 CDMA,Verizon,space gray,64 GB");
		unitInfoDB.put("MG602LL/A", "iPhone 6,A1549 CDMA,Verizon,space gray,128 GB");
		unitInfoDB.put("MG6A2LL/A", "iPhone 6,A1586 CDMA,Sprint,silver,16 GB");
		unitInfoDB.put("MG6H2LL/A", "iPhone 6,A1586 CDMA,Sprint,silver,64 GB");
		unitInfoDB.put("MG6E2LL/A", "iPhone 6,A1586 CDMA,Sprint,silver,128 GB");
		unitInfoDB.put("MG6C2LL/A", "iPhone 6,A1586 CDMA,Sprint,gold,16 GB");
		unitInfoDB.put("MG6J2LL/A", "iPhone 6,A1586 CDMA,Sprint,gold,64 GB");
		unitInfoDB.put("MG6F2LL/A", "iPhone 6,A1586 CDMA,Sprint,gold,128 GB");
		unitInfoDB.put("MG692LL/A", "iPhone 6,A1586 CDMA,Sprint,space gray,16 GB");
		unitInfoDB.put("MG6G2LL/A", "iPhone 6,A1586 CDMA,Sprint,space gray,64 GB");
		unitInfoDB.put("MG6D2LL/A", "iPhone 6,A1586 CDMA,Sprint,space gray,128 GB");
		unitInfoDB.put("MG482B/A", "iPhone 6,A1586 GSM,UK,silver,16 GB");
		unitInfoDB.put("MG4H2B/A", "iPhone 6,A1586 GSM,UK,silver,64 GB");
		unitInfoDB.put("MG4C2B/A", "iPhone 6,A1586 GSM,UK,silver,128 GB");
		unitInfoDB.put("MG492B/A", "iPhone 6,A1586 GSM,UK,gold,16 GB");
		unitInfoDB.put("MG4J2B/A", "iPhone 6,A1586 GSM,UK,gold,64 GB");
		unitInfoDB.put("MG4E2B/A", "iPhone 6,A1586 GSM,UK,gold,128 GB");
		unitInfoDB.put("MG472B/A", "iPhone 6,A1586 GSM,UK,space gray,16 GB");
		unitInfoDB.put("MG4F2B/A", "iPhone 6,A1586 GSM,UK,space gray,64 GB");
		unitInfoDB.put("MG4A2B/A", "iPhone 6,A1586 GSM,UK,space gray,128 GB");
		unitInfoDB.put("MGAM2LL/A", "iPhone 6+,A1522 GSM,ATT,silver,16 GB");
		unitInfoDB.put("MGAV2LL/A", "iPhone 6+,A1522 GSM,ATT,silver,64 GB");
		unitInfoDB.put("MGAQ2LL/A", "iPhone 6+,A1522 GSM,ATT,silver,128 GB");
		unitInfoDB.put("MGAN2LL/A", "iPhone 6+,A1522 GSM,ATT,gold,16 GB");
		unitInfoDB.put("MGAW2LL/A", "iPhone 6+,A1522 GSM,ATT,gold,64 GB");
		unitInfoDB.put("MGAR2LL/A", "iPhone 6+,A1522 GSM,ATT,gold,128 GB");
		unitInfoDB.put("MGAL2LL/A", "iPhone 6+,A1522 GSM,ATT,space gray,16 GB");
		unitInfoDB.put("MGAU2LL/A", "iPhone 6+,A1522 GSM,ATT,space gray,64 GB");
		unitInfoDB.put("MGAP2LL/A", "iPhone 6+,A1522 GSM,ATT,space gray,128 GB");
		unitInfoDB.put("MGC02LL/A", "iPhone 6+,A1522 GSM,T-Mobile,silver,16 GB");
		unitInfoDB.put("MGC62LL/A", "iPhone 6+,A1522 GSM,T-Mobile,silver,64 GB");
		unitInfoDB.put("MGC32LL/A", "iPhone 6+,A1522 GSM,T-Mobile,silver,128 GB");
		unitInfoDB.put("MGC12LL/A", "iPhone 6+,A1522 GSM,T-Mobile,gold,16 GB");
		unitInfoDB.put("MGC72LL/A", "iPhone 6+,A1522 GSM,T-Mobile,gold,64 GB");
		unitInfoDB.put("MGC42LL/A", "iPhone 6+,A1522 GSM,T-Mobile,gold,128 GB");
		unitInfoDB.put("MGAX2LL/A", "iPhone 6+,A1522 GSM,T-Mobile,space gray,16 GB");
		unitInfoDB.put("MGC52LL/A", "iPhone 6+,A1522 GSM,T-Mobile,space gray,64 GB");
		unitInfoDB.put("MGC22LL/A", "iPhone 6+,A1522 GSM,T-Mobile,space gray,128 GB");
		unitInfoDB.put("MGCL2LL/A", "iPhone 6+,A1522 CDMA,Verizon,silver,16 GB");
		unitInfoDB.put("MGCT2LL/A", "iPhone 6+,A1522 CDMA,Verizon,silver,64 GB");
		unitInfoDB.put("MGCP2LL/A", "iPhone 6+,A1522 CDMA,Verizon,silver,128 GB");
		unitInfoDB.put("MGCM2LL/A", "iPhone 6+,A1522 CDMA,Verizon,gold,16 GB");
		unitInfoDB.put("MGCU2LL/A", "iPhone 6+,A1522 CDMA,Verizon,gold,64 GB");
		unitInfoDB.put("MGCQ2LL/A", "iPhone 6+,A1522 CDMA,Verizon,gold,128 GB");
		unitInfoDB.put("MGCK2LL/A", "iPhone 6+,A1522 CDMA,Verizon,space gray,16 GB");
		unitInfoDB.put("MGCR2LL/A", "iPhone 6+,A1522 CDMA,Verizon,space gray,64 GB");
		unitInfoDB.put("MGCN2LL/A", "iPhone 6+,A1522 CDMA,Verizon,space gray,128 GB");
		unitInfoDB.put("MGCW2LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,silver,16 GB");
		unitInfoDB.put("MGD32LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,silver,64 GB");
		unitInfoDB.put("MGD02LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,silver,128 GB");
		unitInfoDB.put("MGCX2LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,gold,16 GB");
		unitInfoDB.put("MGD42LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,gold,64 GB");
		unitInfoDB.put("MGD12LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,gold,128 GB");
		unitInfoDB.put("MGCV2LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,space gray,16 GB");
		unitInfoDB.put("MGD22LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,space gray,64 GB");
		unitInfoDB.put("MGCY2LL/A", "iPhone 6+,A1524 GSM/CDMA,Sprint,space gray,128 GB");
		unitInfoDB.put("MGA92B/A", "iPhone 6+,A1524 GSM,UK,silver,16 GB");
		unitInfoDB.put("MGAJ2B/A", "iPhone 6+,A1524 GSM,UK,silver,64 GB");
		unitInfoDB.put("MGAE2B/A", "iPhone 6+,A1524 GSM,UK,silver,128 GB");
		unitInfoDB.put("MGAA2B/A", "iPhone 6+,A1524 GSM,UK,gold,16 GB");
		unitInfoDB.put("MGAK2B/A", "iPhone 6+,A1524 GSM,UK,gold,64 GB");
		unitInfoDB.put("MGAF2B/A", "iPhone 6+,A1524 GSM,UK,gold,128 GB");
		unitInfoDB.put("MGA82B/A", "iPhone 6+,A1524 GSM,UK,space gray,16 GB");
		unitInfoDB.put("MGAH2B/A", "iPhone 6+,A1524 GSM,UK,space gray,64 GB");
		unitInfoDB.put("MGAC2B/A", "iPhone 6+,A1524 GSM,UK,space gray,128 GB");
		unitInfoDB.put("MGF02LL/A", "iPhone 5C,A1532 GSM,ATT,white,8 GB");
		unitInfoDB.put("ME505LL/A", "iPhone 5C,A1532 GSM,ATT,white,16 GB");
		unitInfoDB.put("MF134LL/A", "iPhone 5C,A1532 GSM,ATT,white,32 GB");
		unitInfoDB.put("MGF42LL/A", "iPhone 5C,A1532 GSM,ATT,pink,8 GB");
		unitInfoDB.put("ME509LL/A", "iPhone 5C,A1532 GSM,ATT,pink,16 GB");
		unitInfoDB.put("MF138LL/A", "iPhone 5C,A1532 GSM,ATT,pink,32 GB");
		unitInfoDB.put("MGF12LL/A", "iPhone 5C,A1532 GSM,ATT,yellow,8 GB");
		unitInfoDB.put("ME506LL/A", "iPhone 5C,A1532 GSM,ATT,yellow,16 GB");
		unitInfoDB.put("MF135LL/A", "iPhone 5C,A1532 GSM,ATT,yellow,32 GB");
		unitInfoDB.put("MGF22LL/A", "iPhone 5C,A1532 GSM,ATT,blue,8 GB");
		unitInfoDB.put("ME507LL/A", "iPhone 5C,A1532 GSM,ATT,blue,16 GB");
		unitInfoDB.put("MF136LL/A", "iPhone 5C,A1532 GSM,ATT,blue,32 GB");
		unitInfoDB.put("MGF32LL/A", "iPhone 5C,A1532 GSM,ATT,green,8 GB");
		unitInfoDB.put("ME508LL/A", "iPhone 5C,A1532 GSM,ATT,green,16 GB");
		unitInfoDB.put("MF137LL/A", "iPhone 5C,A1532 GSM,ATT,green,32 GB");
		unitInfoDB.put("MGF52LL/A", "iPhone 5C,A1532 GSM,T-Mobile,white,8 GB");
		unitInfoDB.put("ME529LL/A", "iPhone 5C,A1532 GSM,T-Mobile,white,16 GB");
		unitInfoDB.put("MF144LL/A", "iPhone 5C,A1532 GSM,T-Mobile,white,32 GB");
		unitInfoDB.put("MGF92LL/A", "iPhone 5C,A1532 GSM,T-Mobile,pink,8 GB");
		unitInfoDB.put("ME533LL/A", "iPhone 5C,A1532 GSM,T-Mobile,pink,16 GB");
		unitInfoDB.put("MF148LL/A", "iPhone 5C,A1532 GSM,T-Mobile,pink,32 GB");
		unitInfoDB.put("MGF62LL/A", "iPhone 5C,A1532 GSM,T-Mobile,yellow,8 GB");
		unitInfoDB.put("ME530LL/A", "iPhone 5C,A1532 GSM,T-Mobile,yellow,16 GB");
		unitInfoDB.put("MF145LL/A", "iPhone 5C,A1532 GSM,T-Mobile,yellow,32 GB");
		unitInfoDB.put("MGF72LL/A", "iPhone 5C,A1532 GSM,T-Mobile,blue,8 GB");
		unitInfoDB.put("ME531LL/A", "iPhone 5C,A1532 GSM,T-Mobile,blue,16 GB");
		unitInfoDB.put("MF146LL/A", "iPhone 5C,A1532 GSM,T-Mobile,blue,32 GB");
		unitInfoDB.put("MGF82LL/A", "iPhone 5C,A1532 GSM,T-Mobile,green,8 GB");
		unitInfoDB.put("ME532LL/A", "iPhone 5C,A1532 GSM,T-Mobile,green,16 GB");
		unitInfoDB.put("MF147LL/A", "iPhone 5C,A1532 GSM,T-Mobile,green,32 GB");
		unitInfoDB.put("MG2E2LL/A", "iPhone 5C,A1532 GSM,US (unlocked),white,8 GB");
		unitInfoDB.put("ME493LL/A", "iPhone 5C,A1532 GSM,US (unlocked),white,16 GB");
		unitInfoDB.put("MF129LL/A", "iPhone 5C,A1532 GSM,US (unlocked),white,32 GB");
		unitInfoDB.put("MG2J2LL/A", "iPhone 5C,A1532 GSM,US (unlocked),pink,8 GB");
		unitInfoDB.put("ME497LL/A", "iPhone 5C,A1532 GSM,US (unlocked),pink,16 GB");
		unitInfoDB.put("MF133LL/A", "iPhone 5C,A1532 GSM,US (unlocked),pink,32 GB");
		unitInfoDB.put("MG2F2LL/A", "iPhone 5C,A1532 GSM,US (unlocked),yellow,8 GB");
		unitInfoDB.put("ME494LL/A", "iPhone 5C,A1532 GSM,US (unlocked),yellow,16 GB");
		unitInfoDB.put("MF130LL/A", "iPhone 5C,A1532 GSM,US (unlocked),yellow,32 GB");
		unitInfoDB.put("MG2G2LL/A", "iPhone 5C,A1532 GSM,US (unlocked),blue,8 GB");
		unitInfoDB.put("ME495LL/A", "iPhone 5C,A1532 GSM,US (unlocked),blue,16 GB");
		unitInfoDB.put("MF131LL/A", "iPhone 5C,A1532 GSM,US (unlocked),blue,32 GB");
		unitInfoDB.put("MG2H2LL/A", "iPhone 5C,A1532 GSM,US (unlocked),green,8 GB");
		unitInfoDB.put("ME496LL/A", "iPhone 5C,A1532 GSM,US (unlocked),green,16 GB");
		unitInfoDB.put("MF132LL/A", "iPhone 5C,A1532 GSM,US (unlocked),green,32 GB");
		unitInfoDB.put("MGFG2LL/A", "iPhone 5C,A1532 CDMA,Verizon,white,8 GB");
		unitInfoDB.put("ME553LL/A", "iPhone 5C,A1532 CDMA,Verizon,white,16 GB");
		unitInfoDB.put("MF154LL/A", "iPhone 5C,A1532 CDMA,Verizon,white,32 GB");
		unitInfoDB.put("MGFL2LL/A", "iPhone 5C,A1532 CDMA,Verizon,pink,8 GB");
		unitInfoDB.put("ME557LL/A", "iPhone 5C,A1532 CDMA,Verizon,pink,16 GB");
		unitInfoDB.put("MF158LL/A", "iPhone 5C,A1532 CDMA,Verizon,pink,32 GB");
		unitInfoDB.put("MGFH2LL/A", "iPhone 5C,A1532 CDMA,Verizon,yellow,8 GB");
		unitInfoDB.put("ME554LL/A", "iPhone 5C,A1532 CDMA,Verizon,yellow,16 GB");
		unitInfoDB.put("MF155LL/A", "iPhone 5C,A1532 CDMA,Verizon,yellow,32 GB");
		unitInfoDB.put("MGFJ2LL/A", "iPhone 5C,A1532 CDMA,Verizon,blue,8 GB");
		unitInfoDB.put("ME555LL/A", "iPhone 5C,A1532 CDMA,Verizon,blue,16 GB");
		unitInfoDB.put("MF156LL/A", "iPhone 5C,A1532 CDMA,Verizon,blue,32 GB");
		unitInfoDB.put("MGFK2LL/A", "iPhone 5C,A1532 CDMA,Verizon,green,8 GB");
		unitInfoDB.put("ME556LL/A", "iPhone 5C,A1532 CDMA,Verizon,green,16 GB");
		unitInfoDB.put("MF157LL/A", "iPhone 5C,A1532 CDMA,Verizon,green,32 GB");
		unitInfoDB.put("MG0Q2CH/A", "iPhone 5C,A1532 CDMA,China Telecom,white,8 GB");
		unitInfoDB.put("MF364CH/A", "iPhone 5C,A1532 CDMA,China Telecom,white,16 GB");
		unitInfoDB.put("MF369CH/A", "iPhone 5C,A1532 CDMA,China Telecom,white,32 GB");
		unitInfoDB.put("MG0V2CH/A", "iPhone 5C,A1532 CDMA,China Telecom,pink,8 GB");
		unitInfoDB.put("MF368CH/A", "iPhone 5C,A1532 CDMA,China Telecom,pink,16 GB");
		unitInfoDB.put("MF373CH/A", "iPhone 5C,A1532 CDMA,China Telecom,pink,32 GB");
		unitInfoDB.put("MG0R2CH/A", "iPhone 5C,A1532 CDMA,China Telecom,yellow,8 GB");
		unitInfoDB.put("MF365CH/A", "iPhone 5C,A1532 CDMA,China Telecom,yellow,16 GB");
		unitInfoDB.put("MF307CH/A", "iPhone 5C,A1532 CDMA,China Telecom,yellow,32 GB");
		unitInfoDB.put("MG0T2CH/A", "iPhone 5C,A1532 CDMA,China Telecom,blue,8 GB");
		unitInfoDB.put("MF366CH/A", "iPhone 5C,A1532 CDMA,China Telecom,blue,16 GB");
		unitInfoDB.put("MF371CH/A", "iPhone 5C,A1532 CDMA,China Telecom,blue,32 GB");
		unitInfoDB.put("MG0U2CH/A", "iPhone 5C,A1532 CDMA,China Telecom,green,8 GB");
		unitInfoDB.put("MF367CH/A", "iPhone 5C,A1532 CDMA,China Telecom,green,16 GB");
		unitInfoDB.put("MF372CH/A", "iPhone 5C,A1532 CDMA,China Telecom,green,32 GB");
		unitInfoDB.put("MGFM2LL/A", "iPhone 5C,A1456 CDMA,Sprint,white,8 GB");
		unitInfoDB.put("ME565LL/A", "iPhone 5C,A1456 CDMA,Sprint,white,16 GB");
		unitInfoDB.put("MF159LL/A", "iPhone 5C,A1456 CDMA,Sprint,white,32 GB");
		unitInfoDB.put("MGFR2LL/A", "iPhone 5C,A1456 CDMA,Sprint,pink,8 GB");
		unitInfoDB.put("ME569LL/A", "iPhone 5C,A1456 CDMA,Sprint,pink,16 GB");
		unitInfoDB.put("MF163LL/A", "iPhone 5C,A1456 CDMA,Sprint,pink,32 GB");
		unitInfoDB.put("MGFN2LL/A", "iPhone 5C,A1456 CDMA,Sprint,yellow,8 GB");
		unitInfoDB.put("ME566LL/A", "iPhone 5C,A1456 CDMA,Sprint,yellow,16 GB");
		unitInfoDB.put("MF160LL/A", "iPhone 5C,A1456 CDMA,Sprint,yellow,32 GB");
		unitInfoDB.put("MGFP2LL/A", "iPhone 5C,A1456 CDMA,Sprint,blue,8 GB");
		unitInfoDB.put("ME567LL/A", "iPhone 5C,A1456 CDMA,Sprint,blue,16 GB");
		unitInfoDB.put("MF161LL/A", "iPhone 5C,A1456 CDMA,Sprint,blue,32 GB");
		unitInfoDB.put("MGFQ2LL/A", "iPhone 5C,A1456 CDMA,Sprint,green,8 GB");
		unitInfoDB.put("ME568LL/A", "iPhone 5C,A1456 CDMA,Sprint,green,16 GB");
		unitInfoDB.put("MF162LL/A", "iPhone 5C,A1456 CDMA,Sprint,green,32 GB");
		unitInfoDB.put("MG8X2B/A", "iPhone 5C,A1507 GSM,UK,white,8 GB");
		unitInfoDB.put("ME499B/A", "iPhone 5C,A1507 GSM,UK,white,16 GB");
		unitInfoDB.put("MF092B/A", "iPhone 5C,A1507 GSM,UK,white,32 GB");
		unitInfoDB.put("MG922B/A", "iPhone 5C,A1507 GSM,UK,pink,8 GB");
		unitInfoDB.put("ME503B/A", "iPhone 5C,A1507 GSM,UK,pink,16 GB");
		unitInfoDB.put("MF096B/A", "iPhone 5C,A1507 GSM,UK,pink,32 GB");
		unitInfoDB.put("MG8Y2B/A", "iPhone 5C,A1507 GSM,UK,yellow,8 GB");
		unitInfoDB.put("ME500B/A", "iPhone 5C,A1507 GSM,UK,yellow,16 GB");
		unitInfoDB.put("MF093B/A", "iPhone 5C,A1507 GSM,UK,yellow,32 GB");
		unitInfoDB.put("MG902B/A", "iPhone 5C,A1507 GSM,UK,blue,8 GB");
		unitInfoDB.put("ME501B/A", "iPhone 5C,A1507 GSM,UK,blue,16 GB");
		unitInfoDB.put("MF094B/A", "iPhone 5C,A1507 GSM,UK,blue,32 GB");
		unitInfoDB.put("MG912B/A", "iPhone 5C,A1507 GSM,UK,green,8 GB");
		unitInfoDB.put("ME502B/A", "iPhone 5C,A1507 GSM,UK,green,16 GB");
		unitInfoDB.put("MF095B/A", "iPhone 5C,A1507 GSM,UK,green,32 GB");
		unitInfoDB.put("MG1Q2CH/A", "iPhone 5C,A1526 GSM,China Unicom,white,8 GB");
		unitInfoDB.put("ME679CH/A", "iPhone 5C,A1526 GSM,China Unicom,white,16 GB");
		unitInfoDB.put("MF104CH/A", "iPhone 5C,A1526 GSM,China Unicom,white,32 GB");
		unitInfoDB.put("MG1W2CH/A", "iPhone 5C,A1526 GSM,China Unicom,pink,8 GB");
		unitInfoDB.put("ME683CH/A", "iPhone 5C,A1526 GSM,China Unicom,pink,16 GB");
		unitInfoDB.put("MF108CH/A", "iPhone 5C,A1526 GSM,China Unicom,pink,32 GB");
		unitInfoDB.put("MG1R2CH/A", "iPhone 5C,A1526 GSM,China Unicom,yellow,8 GB");
		unitInfoDB.put("ME680CH/A", "iPhone 5C,A1526 GSM,China Unicom,yellow,16 GB");
		unitInfoDB.put("MF105CH/A", "iPhone 5C,A1526 GSM,China Unicom,yellow,32 GB");
		unitInfoDB.put("MG1U2CH/A", "iPhone 5C,A1526 GSM,China Unicom,blue,8 GB");
		unitInfoDB.put("ME681CH/A", "iPhone 5C,A1526 GSM,China Unicom,blue,16 GB");
		unitInfoDB.put("MF106CH/A", "iPhone 5C,A1526 GSM,China Unicom,blue,32 GB");
		unitInfoDB.put("MG1V2CH/A", "iPhone 5C,A1526 GSM,China Unicom,green,8 GB");
		unitInfoDB.put("ME682CH/A", "iPhone 5C,A1526 GSM,China Unicom,green,16 GB");
		unitInfoDB.put("MF107CH/A", "iPhone 5C,A1526 GSM,China Unicom,green,32 GB");
		unitInfoDB.put("MG132X/A", "iPhone 5C,A1529 GSM,Australia,white,8 GB");
		unitInfoDB.put("MF321X/A", "iPhone 5C,A1529 GSM,Australia,white,16 GB");
		unitInfoDB.put("MF326X/A", "iPhone 5C,A1529 GSM,Australia,white,32 GB");
		unitInfoDB.put("MG172X/A", "iPhone 5C,A1529 GSM,Australia,pink,8 GB");
		unitInfoDB.put("MF325X/A", "iPhone 5C,A1529 GSM,Australia,pink,16 GB");
		unitInfoDB.put("MF330X/A", "iPhone 5C,A1529 GSM,Australia,pink,32 GB");
		unitInfoDB.put("MG142X/A", "iPhone 5C,A1529 GSM,Australia,yellow,8 GB");
		unitInfoDB.put("MF322X/A", "iPhone 5C,A1529 GSM,Australia,yellow,16 GB");
		unitInfoDB.put("MF327X/A", "iPhone 5C,A1529 GSM,Australia,yellow,32 GB");
		unitInfoDB.put("MG152X/A", "iPhone 5C,A1529 GSM,Australia,blue,8 GB");
		unitInfoDB.put("MF323X/A", "iPhone 5C,A1529 GSM,Australia,blue,16 GB");
		unitInfoDB.put("MF328X/A", "iPhone 5C,A1529 GSM,Australia,blue,32 GB");
		unitInfoDB.put("MG162X/A", "iPhone 5C,A1529 GSM,Australia,green,8 GB");
		unitInfoDB.put("MF324X/A", "iPhone 5C,A1529 GSM,Australia,green,16 GB");
		unitInfoDB.put("MF329X/A", "iPhone 5C,A1529 GSM,Australia,green,32 GB");
		unitInfoDB.put("ME305LL/A", "iPhone 5S,A1533 GSM,ATT,gray,16 GB");
		unitInfoDB.put("ME308LL/A", "iPhone 5S,A1533 GSM,ATT,gray,32 GB");
		unitInfoDB.put("ME311LL/A", "iPhone 5S,A1533 GSM,ATT,gray,64 GB");
		unitInfoDB.put("ME307LL/A", "iPhone 5S,A1533 GSM,ATT,gold,16 GB");
		unitInfoDB.put("ME310LL/A", "iPhone 5S,A1533 GSM,ATT,gold,32 GB");
		unitInfoDB.put("ME313LL/A", "iPhone 5S,A1533 GSM,ATT,gold,64 GB");
		unitInfoDB.put("ME306LL/A", "iPhone 5S,A1533 GSM,ATT,silver,16 GB");
		unitInfoDB.put("ME309LL/A", "iPhone 5S,A1533 GSM,ATT,silver,32 GB");
		unitInfoDB.put("ME312LL/A", "iPhone 5S,A1533 GSM,ATT,silver,64 GB");
		unitInfoDB.put("ME323LL/A", "iPhone 5S,A1533 GSM,T-Mobile,gray,16 GB");
		unitInfoDB.put("ME326LL/A", "iPhone 5S,A1533 GSM,T-Mobile,gray,32 GB");
		unitInfoDB.put("ME329LL/A", "iPhone 5S,A1533 GSM,T-Mobile,gray,64 GB");
		unitInfoDB.put("ME325LL/A", "iPhone 5S,A1533 GSM,T-Mobile,gold,16 GB");
		unitInfoDB.put("ME328LL/A", "iPhone 5S,A1533 GSM,T-Mobile,gold,32 GB");
		unitInfoDB.put("ME331LL/A", "iPhone 5S,A1533 GSM,T-Mobile,gold,64 GB");
		unitInfoDB.put("ME324LL/A", "iPhone 5S,A1533 GSM,T-Mobile,silver,16 GB");
		unitInfoDB.put("ME327LL/A", "iPhone 5S,A1533 GSM,T-Mobile,silver,32 GB");
		unitInfoDB.put("ME330LL/A", "iPhone 5S,A1533 GSM,T-Mobile,silver,64 GB");
		unitInfoDB.put("ME296LL/A", "iPhone 5S,A1533 GSM,US (unlocked),gray,16 GB");
		unitInfoDB.put("ME299LL/A", "iPhone 5S,A1533 GSM,US (unlocked),gray,32 GB");
		unitInfoDB.put("ME302LL/A", "iPhone 5S,A1533 GSM,US (unlocked),gray,64 GB");
		unitInfoDB.put("ME298LL/A", "iPhone 5S,A1533 GSM,US (unlocked),gold,16 GB");
		unitInfoDB.put("ME301LL/A", "iPhone 5S,A1533 GSM,US (unlocked),gold,32 GB");
		unitInfoDB.put("ME304LL/A", "iPhone 5S,A1533 GSM,US (unlocked),gold,64 GB");
		unitInfoDB.put("ME297LL/A", "iPhone 5S,A1533 GSM,US (unlocked),silver,16 GB");
		unitInfoDB.put("ME300LL/A", "iPhone 5S,A1533 GSM,US (unlocked),silver,32 GB");
		unitInfoDB.put("ME303LL/A", "iPhone 5S,A1533 GSM,US (unlocked),silver,64 GB");
		unitInfoDB.put("ME341LL/A", "iPhone 5S,A1533 CDMA,Verizon,gray,16 GB");
		unitInfoDB.put("ME344LL/A", "iPhone 5S,A1533 CDMA,Verizon,gray,32 GB");
		unitInfoDB.put("ME347LL/A", "iPhone 5S,A1533 CDMA,Verizon,gray,64 GB");
		unitInfoDB.put("ME343LL/A", "iPhone 5S,A1533 CDMA,Verizon,gold,16 GB");
		unitInfoDB.put("ME346LL/A", "iPhone 5S,A1533 CDMA,Verizon,gold,32 GB");
		unitInfoDB.put("ME349LL/A", "iPhone 5S,A1533 CDMA,Verizon,gold,64 GB");
		unitInfoDB.put("ME342LL/A", "iPhone 5S,A1533 CDMA,Verizon,silver,16 GB");
		unitInfoDB.put("ME345LL/A", "iPhone 5S,A1533 CDMA,Verizon,silver,32 GB");
		unitInfoDB.put("ME348LL/A", "iPhone 5S,A1533 CDMA,Verizon,silver,64 GB");
		unitInfoDB.put("MF381CH/A", "iPhone 5S,A1533 CDMA,China Telecom,gray,16 GB");
		unitInfoDB.put("MF384CH/A", "iPhone 5S,A1533 CDMA,China Telecom,gray,32 GB");
		unitInfoDB.put("MF387CH/A", "iPhone 5S,A1533 CDMA,China Telecom,gray,64 GB");
		unitInfoDB.put("MF383CH/A", "iPhone 5S,A1533 CDMA,China Telecom,gold,16 GB");
		unitInfoDB.put("MF386CH/A", "iPhone 5S,A1533 CDMA,China Telecom,gold,32 GB");
		unitInfoDB.put("MF389CH/A", "iPhone 5S,A1533 CDMA,China Telecom,gold,64 GB");
		unitInfoDB.put("MF382CH/A", "iPhone 5S,A1533 CDMA,China Telecom,silver,16 GB");
		unitInfoDB.put("MF385CH/A", "iPhone 5S,A1533 CDMA,China Telecom,silver,32 GB");
		unitInfoDB.put("MF388CH/A", "iPhone 5S,A1533 CDMA,China Telecom,silver,64 GB");
		unitInfoDB.put("ME450CH/A", "iPhone 5S,A1528 GSM,China Unicom,gray,16 GB");
		unitInfoDB.put("ME453CH/A", "iPhone 5S,A1528 GSM,China Unicom,gray,32 GB");
		unitInfoDB.put("ME456CH/A", "iPhone 5S,A1528 GSM,China Unicom,gray,64 GB");
		unitInfoDB.put("ME452CH/A", "iPhone 5S,A1528 GSM,China Unicom,gold,16 GB");
		unitInfoDB.put("ME455CH/A", "iPhone 5S,A1528 GSM,China Unicom,gold,32 GB");
		unitInfoDB.put("ME458CH/A", "iPhone 5S,A1528 GSM,China Unicom,gold,64 GB");
		unitInfoDB.put("ME451CH/A", "iPhone 5S,A1528 GSM,China Unicom,silver,16 GB");
		unitInfoDB.put("ME454CH/A", "iPhone 5S,A1528 GSM,China Unicom,silver,32 GB");
		unitInfoDB.put("ME457CH/A", "iPhone 5S,A1528 GSM,China Unicom,silver,64 GB");
		unitInfoDB.put("ME350LL/A", "iPhone 5S,A1453 CDMA,Sprint,gray,16 GB");
		unitInfoDB.put("ME353LL/A", "iPhone 5S,A1453 CDMA,Sprint,gray,32 GB");
		unitInfoDB.put("ME356LL/A", "iPhone 5S,A1453 CDMA,Sprint,gray,64 GB");
		unitInfoDB.put("ME352LL/A", "iPhone 5S,A1453 CDMA,Sprint,gold,16 GB");
		unitInfoDB.put("ME355LL/A", "iPhone 5S,A1453 CDMA,Sprint,gold,32 GB");
		unitInfoDB.put("ME358LL/A", "iPhone 5S,A1453 CDMA,Sprint,gold,64 GB");
		unitInfoDB.put("ME351LL/A", "iPhone 5S,A1453 CDMA,Sprint,silver,16 GB");
		unitInfoDB.put("ME354LL/A", "iPhone 5S,A1453 CDMA,Sprint,silver,32 GB");
		unitInfoDB.put("ME357LL/A", "iPhone 5S,A1453 CDMA,Sprint,silver,64 GB");
		unitInfoDB.put("ME432B/A", "iPhone 5S,A1457 GSM,UK,gray,16 GB");
		unitInfoDB.put("ME435B/A", "iPhone 5S,A1457 GSM,UK,gray,32 GB");
		unitInfoDB.put("ME438B/A", "iPhone 5S,A1457 GSM,UK,gray,64 GB");
		unitInfoDB.put("ME434B/A", "iPhone 5S,A1457 GSM,UK,gold,16 GB");
		unitInfoDB.put("ME437B/A", "iPhone 5S,A1457 GSM,UK,gold,32 GB");
		unitInfoDB.put("ME440B/A", "iPhone 5S,A1457 GSM,UK,gold,64 GB");
		unitInfoDB.put("ME433B/A", "iPhone 5S,A1457 GSM,UK,silver,16 GB");
		unitInfoDB.put("ME436B/A", "iPhone 5S,A1457 GSM,UK,silver,32 GB");
		unitInfoDB.put("ME439B/A", "iPhone 5S,A1457 GSM,UK,silver,64 GB");
		unitInfoDB.put("MF352X/A", "iPhone 5S,A1530 GSM,Australia,gray,16 GB");
		unitInfoDB.put("MF355X/A", "iPhone 5S,A1530 GSM,Australia,gray,32 GB");
		unitInfoDB.put("MF358X/A", "iPhone 5S,A1530 GSM,Australia,gray,64 GB");
		unitInfoDB.put("MF354X/A", "iPhone 5S,A1530 GSM,Australia,gold,16 GB");
		unitInfoDB.put("MF357X/A", "iPhone 5S,A1530 GSM,Australia,gold,32 GB");
		unitInfoDB.put("MF360X/A", "iPhone 5S,A1530 GSM,Australia,gold,64 GB");
		unitInfoDB.put("MF353X/A", "iPhone 5S,A1530 GSM,Australia,silver,16 GB");
		unitInfoDB.put("MF356X/A", "iPhone 5S,A1530 GSM,Australia,silver,32 GB");
		unitInfoDB.put("MF359X/A", "iPhone 5S,A1530 GSM,Australia,silver,64 GB");
		unitInfoDB.put("MD634LL/A", "iPhone 5,A1428 GSM,ATT,black,16 GB");
		unitInfoDB.put("MD636LL/A", "iPhone 5,A1428 GSM,ATT,black,32 GB");
		unitInfoDB.put("MD644LL/A", "iPhone 5,A1428 GSM,ATT,black,64 GB");
		unitInfoDB.put("MD635LL/A", "iPhone 5,A1428 GSM,ATT,silver,16 GB");
		unitInfoDB.put("MD637LL/A", "iPhone 5,A1428 GSM,ATT,silver,32 GB");
		unitInfoDB.put("MD645LL/A", "iPhone 5,A1428 GSM,ATT,silver,64 GB");
		unitInfoDB.put("MD293LL/A", "iPhone 5,A1428 GSM,US (unlocked),black,16 GB");
		unitInfoDB.put("MD295LL/A", "iPhone 5,A1428 GSM,US (unlocked),black,32 GB");
		unitInfoDB.put("MD642LL/A", "iPhone 5,A1428 GSM,US (unlocked),black,64 GB");
		unitInfoDB.put("MD294LL/A", "iPhone 5,A1428 GSM,US (unlocked),silver,16 GB");
		unitInfoDB.put("MD296LL/A", "iPhone 5,A1428 GSM,US (unlocked),silver,32 GB");
		unitInfoDB.put("MD643LL/A", "iPhone 5,A1428 GSM,US (unlocked),silver,64 GB");
		unitInfoDB.put("MD656LL/A", "iPhone 5,A1429 CDMA,Sprint,black,16 GB");
		unitInfoDB.put("MD660LL/A", "iPhone 5,A1429 CDMA,Sprint,black,32 GB");
		unitInfoDB.put("MD667LL/A", "iPhone 5,A1429 CDMA,Sprint,black,64 GB");
		unitInfoDB.put("MD657LL/A", "iPhone 5,A1429 CDMA,Sprint,silver,16 GB");
		unitInfoDB.put("MD661LL/A", "iPhone 5,A1429 CDMA,Sprint,silver,32 GB");
		unitInfoDB.put("MD668LL/A", "iPhone 5,A1429 CDMA,Sprint,silver,64 GB");
		unitInfoDB.put("MD654LL/A", "iPhone 5,A1429 CDMA,Verizon,black,16 GB");
		unitInfoDB.put("MD658LL/A", "iPhone 5,A1429 CDMA,Verizon,black,32 GB");
		unitInfoDB.put("MD664LL/A", "iPhone 5,A1429 CDMA,Verizon,black,64 GB");
		unitInfoDB.put("MD655LL/A", "iPhone 5,A1429 CDMA,Verizon,silver,16 GB");
		unitInfoDB.put("MD659LL/A", "iPhone 5,A1429 CDMA,Verizon,silver,32 GB");
		unitInfoDB.put("MD665LL/A", "iPhone 5,A1429 CDMA,Verizon,silver,64 GB");
		unitInfoDB.put("MD297B/A", "iPhone 5,A1429 GSM,UK,black,16 GB");
		unitInfoDB.put("MD299B/A", "iPhone 5,A1429 GSM,UK,black,32 GB");
		unitInfoDB.put("MD662B/A", "iPhone 5,A1429 GSM,UK,black,64 GB");
		unitInfoDB.put("MD298B/A", "iPhone 5,A1429 GSM,UK,silver,16 GB");
		unitInfoDB.put("MD300B/A", "iPhone 5,A1429 GSM,UK,silver,32 GB");
		unitInfoDB.put("MD663B/A", "iPhone 5,A1429 GSM,UK,silver,64 GB");
		unitInfoDB.put("ME039CH/A", "iPhone 5,A1442 CDMA,China Telecom,black,16 GB");
		unitInfoDB.put("ME041CH/A", "iPhone 5,A1442 CDMA,China Telecom,black,32 GB");
		unitInfoDB.put("ME043CH/A", "iPhone 5,A1442 CDMA,China Telecom,black,64 GB");
		unitInfoDB.put("ME040CH/A", "iPhone 5,A1442 CDMA,China Telecom,silver,16 GB");
		unitInfoDB.put("ME042CH/A", "iPhone 5,A1442 CDMA,China Telecom,silver,32 GB");
		unitInfoDB.put("ME044CH/A", "iPhone 5,A1442 CDMA,China Telecom,silver,64 GB");
		unitInfoDB.put("ME486LL/A", "iPhone 5,A1428 GSM,T-Mobile,black,16 GB");
		unitInfoDB.put("ME488LL/A", "iPhone 5,A1428 GSM,T-Mobile,black,32 GB");
		unitInfoDB.put("ME490LL/A", "iPhone 5,A1428 GSM,T-Mobile,black,64 GB");
		unitInfoDB.put("ME487LL/A", "iPhone 5,A1428 GSM,T-Mobile,silver,16 GB");
		unitInfoDB.put("ME489LL/A", "iPhone 5,A1428 GSM,T-Mobile,silver,32 GB");
		unitInfoDB.put("ME491LL/A", "iPhone 5,A1428 GSM,T-Mobile,silver,64 GB");

	}
	
	public String getUnitInfo(String modelNo) {
		if (!unitInfoDB.containsKey(modelNo)) {
			return "";
		}
		return unitInfoDB.get(modelNo);
	}
	
}

