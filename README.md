# iPhone Seller Tool

## Install
There are a few different ways you can install this tool :

* Go to Google Play Market and 

* Clone the source: `git clone git://github.com/x1angli/` 
* Make sure your Java, Maven, and Android SDK are properly installed
* Go to the directory, and execute following commands consecutively
	mvn release:prepare
	mvn release:perform

## Debug
* Clone the source: `git clone git://github.com/x1angli/` 
* Make sure your Java, Maven, and Android SDK are properly installed
* Go to the directory, and execute `mvn package`


(to be continued)
